echo 'current dir is '
pwd
export http_proxy=http://proxy.esrf.fr:3128/
export https_proxy=http://proxy.esrf.fr:3128/
export no_proxy='localhost,127.0.0.1,localaddress,.esrf.fr,.esrf.eu '
unset PYTHONPATH
export PATH=/usr/local/bin:/usr/bin:/bin:/opt/oar/utilities:/sbin:/usr/bin/X11:.::.
unset LDFLAGS
unset CPPFLAGS
unset LD_LIBRARY_PATH

rm -rf /tmp/chst_test/
python setup.py install --prefix=/tmp/chst_test
export PYTHONPATH=/tmp/chst_test/lib/python2.7/site-packages
export PATH=/tmp/chst_test/bin:$PATH
python run_tests.py -v --testcase oargpu
