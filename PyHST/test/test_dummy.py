#!/usr/bin/env python
# coding: utf-8
#
#    Project: Azimuthal integration
#             https://github.com/silx-kit/pyFAI
#
#    Copyright (C) 2015-2018 European Synchrotron Radiation Facility, Grenoble, France
#
#    Principal author:       Jérôme Kieffer (Jerome.Kieffer@ESRF.eu)
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

"""Dummy test to run first to check for relative imports
"""

from __future__ import absolute_import, division, print_function


__author__ = "Jérôme Kieffer"
__contact__ = "Jerome.Kieffer@ESRF.eu"
__license__ = "MIT"
__copyright__ = "European Synchrotron Radiation Facility, Grenoble, France"
__date__ = "15/03/2018"

import subprocess

import unittest
import sys
import logging
from .utilstest import UtilsTest
import os
import glob
logger = logging.getLogger(__name__)


def get_project_name(root_dir):
    """Retrieve project name by running python setup.py --name in root_dir.

    :param str root_dir: Directory where to run the command.
    :return: The name of the project stored in root_dir
    """
    logger.debug("Getting project name in %s", root_dir)
    
    exists = os.path.isfile(os.path.join( root_dir,   "setup.py"    )   )
    if exists :
        p = subprocess.Popen([sys.executable, "setup.py", "--name"],
                             shell=False, cwd=root_dir, stdout=subprocess.PIPE)
        name, _stderr_data = p.communicate()
        logger.debug("subprocess ended with rc= %s", p.returncode)
        print(" PROJECT ", name ) 
        print(" PROJECT ", name.split()[-1].decode('ascii') ) 
        return name.split()[-1].decode('ascii')
    else:
        fns = glob.glob(os.path.join(root_dir,"*"))
        print( "=======   ", fns)
        for n in fns:
            if "PyHST2_" in n :
                index = n.find("PyHST2_")
                n =n[index:]
                return n
    raise Exception( " unable to find project name "   )
        
                        

class TestDummy(unittest.TestCase):
    def test_dummy(self):
        PROJECT = get_project_name(".")
        if PROJECT not in sys.modules:
            PyHST  = __import__(PROJECT  )
        else:
            PyHST  =   sys.modules[PROJECT]
        logger.info("here is project  %s",PROJECT )


def suite(testcase):
    mytests=["standard","nslino"]
    testsuite = unittest.TestSuite()
    if testcase in mytests:
        loader = unittest.defaultTestLoader.loadTestsFromTestCase
        testsuite.addTest(loader(TestDummy))
        print( " ===== > " , testsuite) 
    return testsuite


if __name__ == '__main__':
    runner = unittest.TextTestRunner()
    runner.run(suite())
    UtilsTest.clean_up()
