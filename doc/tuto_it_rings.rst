.. include:: texenv.txt

Iterative rings artifacts correction
=====================================

About
------

PyHST2 implements a new rings correction algorithm described `here <http://scripts.iucr.org/cgi-bin/paper?S1600577515010176>`_.
This method performs an iterative rings removal, thus, it is especially interesting if you are using an iterative reconstruction.
If you are *not* using an iterative reconstruction, you should first consider standard correction methods (sinogram pre-processing and slice post-processing) !


Basic usage
------------

The parameters enabling the rings correction are the following :

.. code-block:: python

    ITER_RING_HEIGHT = 50000 # Put a huge value
    ITER_RING_SIZE = 1  # Only relevant for DL
    ITER_RING_BETA = 0.5 # Weight of the "sparsity" of the rings

The parameter ``ITER_RING_HEIGHT``, when different from zero, enables the iterative rings correction.
The parameter ``ITER_RING_BETA`` weights the sparsity of the rings which are substracted to the sinogram.
For high values, the rings are expected to be narrow. For low values, the rings are expected to be large.

The parameter ``ITER_RING_SIZE`` is only relevant for Dictionary-based reconstruction. It is the expected size of the rings. Typical values are 1 or 2.


Partial rings
--------------

For Dictionary-based reconstruction, this method has been extended for partial rings, i.e artifacts which do not have the same "intensity" over 360 degrees.

.. code-block:: python

    NUMBER_OF_RINGS = 1 # When using partial rings correction (only for DL)

This parameter indicates that the 360° circle will be split in ``NUMBER_OF_RINGS`` parts.

**Warning** : this feature is still experimental !
