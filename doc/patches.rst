DICTIONARY LEARNING
===================
  
 *The following documentation has been extracted automatically from the comments found in the source code. Discard Parameters. object variable.*
   

 .. automodule:: Parameters_module
     :noindex:
 .. autoclass:: Parameters
     :members: ITERATIVE_CORRECTIONS,DO_PRECONDITION, FISTA,DENOISING_TYPE, BETA_TV, N_ITERS_DENOISING,PATCHES_FILE,STEPFORPATCHES,WEIGHT_OVERLAP, VECTORIALITY, OPTIM_ALGORITHM


