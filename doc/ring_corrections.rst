Ring Corrections
=====================


 *The following documentation has been extracted automatically from the comments found in the source code. Discard Parameters. object variable.*
   

 .. automodule:: Parameters_module
     :noindex:
 .. autoclass:: Parameters
     :members: DO_RING_FILTER, RING_FILTER, RING_FILTER_PARA, FW_LEVELS, FW_SIGMA, FW_WNAME, FW_SCALING, FW_FILTERPARAM, DOUBLEFFCORRECTION_ONTHEFLY, FF2_SIGMA
	       
 
 *The following items are operative only with iterative methods
   

 .. automodule:: Parameters_module
     :noindex:
 .. autoclass:: Parameters
     :members:  ITER_RING_HEIGHT, ITER_RING_SIZE, ITER_RING_BETA, RING_ALPHA, DO_DUMP_HRINGS , INTERVALS_HRINGS
	       
 
