Preprocessing Workflow
======================

For each projection

* SUBTRACT_BACKGROUND
* CORRECT_FLATFIELD
    a division
* ZEROCLIPVALUE and ONECLIPVALUE
* DOUBLEFFCORRECTION
   * if logarithm is to be taken afterword : multiplication by the exponential of the doubleFF data
   * otherwise subtraction of the doubleFF data from the projection
* DO_CCD_FILTER
* DO_PAGANIN
* Longitudinal axis correction if they are contained in AXIS_CORRECTION_FILE (second column)
* TAKE_LOGARITHM [TAKE_LOGARITHM]

