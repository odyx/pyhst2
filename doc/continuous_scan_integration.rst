Continuous Scan Integration
===========================
  
 *The following documentation has been extracted automatically from the comments found in the source code. Discard Parameters. object variable.*
   

 .. automodule:: Parameters_module
     :noindex:
 .. autoclass:: Parameters
     :members: DETECTOR_DUTY_RATIO,  DETECTOR_DUTY_OVERSAMPLING
 
