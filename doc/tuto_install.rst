Installation of PyHST2
=======================

This tutorial explains how to install and run the latest version of PyHST2.
You do not need the root privileges to install PyHST2.

Compilation and installation
-----------------------------

Dependencies
##############

To install and run PyHST, you need to install the following :
    * Software: python, cython, gcc, nvidia-cuda-toolkit
    * Libraries: HDF5 (libhdf5-dev), openMPI, FFTW3, OpenMP

Please not that **PyHST is not intended for running on Windows**.


Local installation
####################

In this tutorial, we will suppose that PyHST2 will be installed in a custom directory : **/home/myself/software/pyhst2**.


First, create the directory in which PyHST2 will be installed.
In this directory, retrieve the source of the latest version of PyHST2 from the repository :

.. code-block:: bash

    cd software
    mkdir pyhst2
    cd pyhst2
    git clone https://gitlab.esrf.fr/mirone/pyhst2

the cloned directory name is ``pyhst2``.

The next step is to prepare the directory where the binaries will be installed. By default, this directory is ``~/packages``, but here we will do it manually.

.. code-block:: bash

    mkdir bin
    cd bin
    export PYHST2_BIN=$(pwd)

Here the environment variable ``PYHST2_BIN`` contains ``/home/myself/software/pyhst2/bin``.
Then, go into the folder containing the sources (cloned from repository) and launch the compilation :

.. code-block:: bash

    cd ../pyhst2
    python setup.py install --prefix=$PYHST2_BIN

For the default installation, you can omit the ``--prefix=$PYHST2_BIN`` option.

After the compilation, the last thing to do is to specify the installation folder to be able to run PyHST :

.. code-block:: bash

    export PATH=$PATH:$PYHST2_BIN/bin
    export PYTHONPATH=$PYTHONPATH:$PYHST2_BIN/lib/python2.7/site-packages



Launching PyHST2
-----------------

Setting the environment variables
##################################

Each time you open a new bash session, you need to set the environment variable of the PyHST2 directory.
For the custom installation in ``/home/myself/software/pyhst2/`` :

.. code-block:: bash

    # For custom-directory installation
    export PATH=$PATH:/home/myself/software/pyhst2/bin/bin
    export PYTHONPATH=$PYTHONPATH:/home/myself/software/pyhst2/bin/lib/python2.7/site-packages

For the standard installation (no ``--prefix`` was specified during the installation) :

.. code-block:: bash

    # For standard installation
    export PATH=$PATH:/home/myself/packages/bin
    export PYTHONPATH=$PYTHONPATH:/home/myself/packages/lib/python2.7/site-packages

Then, when you type ``PyHST2_`` and press [TAB] in a terminal, the current version should appear as a runnable command.


The launch syntax
##################

If PyHST2 is **not** run from an OAR session, you need to specify :
    * the parameter file
    * the machine name

so an example would be :

.. code-block:: bash

    PyHST2_2015X inputfile.par machine,0

Here, ``machine`` is the machine name, which is the second word of the result of the ``uname -a`` command.
The ``,0`` option is the GPU number to use.

If PyHST2 is run from an OAR session, you do not need to specify the machine name :

.. code-block:: bash

    PyHST2_2015X inputfile.par

Do not forget to **request a GPU** when opening an OAR session.
For example, an interactive session of 10 minutes with a GPU can be obtained with

.. code-block:: bash

    oarsub -I -l "nodes=1,walltime=10:00" -p "gpu='YES'"



About versions
---------------

PyHST2 uses the format ``PyHST2_2015X`` for the version name (for example ``PyHST2_2015a``, ``PyHST2_2015b``, ...).
