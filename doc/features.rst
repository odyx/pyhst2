Features
========

Contents:

.. toctree::
   :maxdepth: 2
	   
   ring_corrections
   ccd_corrections
   axis_corrections
   beam_hardening_corrections	      
   paganin
   conical_geometry
   helical_geometry
   iterative_corrections
   multi_paganin
   third_party_plugins
   tuto_it_sirtfilter
   volume_projection
   
