Non Local Means
===============
  
 *The following documentation has been extracted automatically from the comments found in the source code. Discard Parameters. object variable.*
   

 .. automodule:: Parameters_module
     :noindex:
 .. autoclass:: Parameters
     :members: ITERATIVE_CORRECTIONS,DENOISING_TYPE,BETA_TV, CALM_ZONE_LEN, NLM_NOISE_GEOMETRIC_RATIO, NLM_NOISE_INITIAL_FACTOR

 
