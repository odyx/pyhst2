AXIS Corrections
=====================


 *The following documentation has been extracted automatically from the comments found in the source code. Discard Parameters. object variable.*
   

 .. automodule:: Parameters_module
     :noindex:
 .. autoclass:: Parameters
     :members: DO_AXIS_CORRECTION, AXIS_CORRECTION_FILE
	       
 
