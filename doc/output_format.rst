Output Format
=============

The output format is determined by the **OUTPUT_FILE**  input variable

  *The following documentation has been generated automatically from the comments found in the code.*

.. automodule:: Parameters_module

.. autoclass:: Parameters
    :members: OUTPUT_FILE, OUTPUT_SINOGRAMS, DO_OUTPUT_PAGANIN


