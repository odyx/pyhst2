
All Input Variables
===================

  The input variables and their value are written in the input file using a python syntax.
  The input file is indeed executed as a python script.

  The format is like this ::

    var1=value1
    var2=value2
    ....

  The python indenting starts from the first column.

  *The following documentation has been extracted automatically from the comments found in the source code. Discard Parameters. object variable.*
   

 .. automodule:: Parameters_module
     :noindex:
 .. autoclass:: Parameters
     :members:
     :noindex:

 
