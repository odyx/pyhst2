
.. _multipaganin:

Multi Paganin
=============
  

For sample like flesh with bones it is possible to trigger automatically 
the removal of the bone signal from the data. The filtered data are then
(always automatically) reconstructed with the appropriate Paganin parameters
for the flesh. A tutorial based on the provided nonregressions tests is
givene here :ref:`tuto-multipaganin`.
The final image is a collage where the bones are sticked above from another reconstruction
with bones appropriate parameters.

The basic usage is the following. Supposing the the par file already contains the parameters appropriate for 
Paganin reconstruction of flesh, you have to add at the bottom ::

 MULTI_PAGANIN_PARS={}
 MULTI_PAGANIN_PARS["BONE_Lmicron"]= 1023.083771
 MULTI_PAGANIN_PARS["THRESHOLD"] = 0.32
 MULTI_PAGANIN_PARS["DILATE"] = 2
 MULTI_PAGANIN_PARS["MEDIANR"] = 4

The above steps are all what is required for the basic usage.
An optional keyword ::

  MULTI_PAGANIN_PARS["REPLACEMENT"]

is by default zero. If it is zero the bone correction will be taken with respect to an intrapolated background
that diffused from the boundaries of the masked regions: a sort of average of the values found at the border.
If it is instead different from zero ( if it is set in the input file) then
no diffusion is done but the value is fixed by the given  parameter.



The reconstruction passes through several intermediated steps, with temporary files
in /tmp , with automatic random names, which are afterwords canceled.

You can redirect the intermediate data on files of your choice for inspection.
To do this you can set a selection or the whole of the following options (or just add nothing if you are not interested in the intermediate steps) ::

  MULTI_PAGANIN_PARS["PAGBONE_FILE"]= "pagbone.vol"
  MULTI_PAGANIN_PARS["MASKBONE_FILE"]= "maskbone.vol"
  MULTI_PAGANIN_PARS["ABSBONE_FILE"]= "absbone.vol"
  MULTI_PAGANIN_PARS["CORRBONE_FILE"]= "corrbone.vol"
  MULTI_PAGANIN_PARS["MASKBONE_FILE"] = "maskbone.vol"
  MULTI_PAGANIN_PARS["CORRPROJSPATTERN"] =  "/tmp/tmppnohHx.vol/proj_%05d.edf"
  MULTI_PAGANIN_PARS["CORRECTEDVOL_FILE"] = "corrected.vol"

These files, when they are set by the user and not named automatically in absence of the option,
 will not be canceled after usage.

 The intermediate files are  generated/used in the following steps:

  *  STEP 1 :  generates pagbone.vol with the Paganin for bones
  *  STEP 2 :  generates maskbone.vol from pagbone.vol according to the threshold, the radius for the median filter window and the dilatation
  *  STEP 3 :  generates the absorption reconstruction in absbone.vol
  *  STEP 4 :  generates corrbone.vol from  maskbone.vol and   absbone.vol. It contains the mask times (absbone.vol -average_around)
  *  STEP 5 :  generates a set of projections in CORRPROJSPATTERN like files  from corrbone.vol
  *  STEP 6 :  generates corrected.vol from the original data,  but the synthetised projections are used to correct the radiographies before applying Paganin.
  *  STEP 7 :  Produces the final result merging together corrected.vol and pagbone.vol where maskbone.vol is 1.

You can perform selected steps giving the option ::

 *  MULTI_PAGANIN_PARS["STEPS"]= [1,1,1,1,1,1,1]

whose value is a list with seven integers which can be 0 or 1.
If a step is activated and previous ones are not, the required files for the selected
steps must be already there from a previous run.


Your can select the temporary directory. Its default is /tmp ::

   MULTI_PAGANIN_PARS["TMPDIR"]= "/scratch"



