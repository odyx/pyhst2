Performance TUning  Options
===========================

You can safely let this options to theur default value.

 *The following documentation has been extracted automatically from the comments found in the source code. Discard Parameters. object variable.*
   

 .. automodule:: Parameters_module
     :noindex:

 .. autoclass:: Parameters
     :members:   TRYGPUCCDFILTER, NSLICESATONCE, NPBUNCHES,ncpus_expansion,   MAXNTOKENS , TRYEDFCONSTANTHEADER , RAWDATA_MEMORY_REUSE , JOSEPHNOCLIP

	       
 
