PAGANIN
=======

You find the the example in ::

/scisoft/ESRF_sw/TEST_PYHST/CRAYON


.. image:: datas/examples/paganin/crayon.png
    :width:  300
    :height: 300

Input File:

.. literalinclude::  datas/examples/paganin/ex_paganin.par
    :language: python

