
import sys, os
import numpy, h5py

class Converter(object):
    def __init__(self, infile):
        self.infile = infile
        info = infile + ".info"
        if os.path.exists(info):
            self.info = self.read_info(info)
        else:
            self.info = {}

    def read_info(self, infofile):
        """
        Read an info file containing data
        
        @param infofile: filename of .info
        @return: dict with parameters
        """
        dico = {}
        with open(infofile) as f:
            for line in f:
                words = [i.strip() for i in line.split("=")]
                if len(words) == 2:
                    if words[1].isdigit():
                        dico[words[0]] = int(words[ 1])
                    else:
                        try:
                            dico[words[0]] = float(words[ 1])
                        except ValueError:
                            dico[words[0]] = words[ 1]
        return dico
    def save(self, outfile, path="data"):
        sys.stdout.write("Converting volume to HDF5. Frame 0000")
        dtype = numpy.float32
        h5 = h5py.File(outfile)
        if path in h5:
            del h5[path]
        shape = [self.info[i] for i in ('NUM_Z', 'NUM_Y', "NUM_X")]
        chunks = (1, self.info['NUM_Y'], self.info["NUM_X"])
        ds = h5.create_dataset(path, shape=shape, dtype=dtype, chunks=chunks)
        block_size = self.info['NUM_Y'] * self.info["NUM_X"] * numpy.float32(1).itemsize
        with open(self.infile) as ff:
            for i in range(self.info['NUM_Z']):
                block = ff.read(block_size)
                data = numpy.fromstring(block, dtype=dtype)
                data.shape = self.info['NUM_Y'], self.info["NUM_X"]
                ds[i, :, :] = data
                sys.stdout.write("\b"*4 + "%04i" % (i + 1))
        print(" Done")

def main():
    path = "data"
    if len(sys.argv) == 2:
        infile = sys.argv[1]
        outfile = os.path.splitext(infile)[0] + ".h5"
    elif len(sys.argv) == 3:
        infile = sys.argv[1]
        outfile = sys.argv[2]
    elif len(sys.argv) == 4:
        infile = sys.argv[1]
        outfile = sys.argv[2]
        path = sys.argv[3]
    else:
        print(__doc__)
        sys.exit(-1)
    conv = Converter(infile)
    conv.save(outfile, path)
if __name__ == "__main__":
    main()
	
